package BaiTapGeneric.Bai1;

public class HinhVuong implements IHinhHoc{
    private double canh;

    public HinhVuong() {
    }

    public HinhVuong(double canh) {
        this.canh = canh;
    }

    public double getCanh() {
        return canh;
    }

    public void setCanh(double canh) {
        this.canh = canh;
    }


    @Override
    public double tinhChuVi() {
        double cv = getCanh()*4;
        return cv;
    }

    @Override
    public double tinhDienTich() {
        double dt= getCanh()*getCanh();
        return dt;
    }

    @Override
    public String toString() {
        return "HinhVuong{" +
                "canh=" + canh +
                '}';
    }
}


