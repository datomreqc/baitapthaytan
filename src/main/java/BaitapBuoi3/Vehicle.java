package BaitapBuoi3;

public class Vehicle {
    int dungTich;
    double triGia;
    String hangXe,chuXe,mauXe;

    public Vehicle( int dungTich, double triGia, String chuXe, String mauXe,String hangXe) {
        this.hangXe = hangXe;
        this.dungTich = dungTich;
        this.triGia = triGia;
        this.chuXe = chuXe;
        this.mauXe = mauXe;
    }
    public Vehicle() {

    }
    public String  getHangXe() {
        return hangXe;
    }
    public int getDungTich() {
        return dungTich;
    }
    public String getChuXe() {
        return chuXe;
    }
    public String getMauXe() {
        return mauXe;
    }
    public double getTriGia() {
        return triGia;
    }
    public void setDungTich(int dungTich) {
        this.dungTich = dungTich;
    }
    public void setHangXe(String hangXe) {
        this.hangXe = hangXe;
    }
    public void setChuXe(String chuXe) {
        this.chuXe = chuXe;
    }
    public void setMauXe(String mauXe) {
        this.mauXe = mauXe;
    }
    public void setTriGia(double triGia) {
        this.triGia = triGia;
    }

    public double TienThue() {
        if(dungTich >= 0 && dungTich < 100) {
            return triGia*0.01;
        }else if (dungTich >= 100 && dungTich <200) {
            return triGia*0.03;
        }else {
            return triGia*0.05;
        }
    }

    public String toString(){//ghi de phuong thuc toString()
        return chuXe+" "+hangXe+" "+ dungTich+" "+ triGia + " "+mauXe+ " " +TienThue();
    }
    public void inThue() {
        System.out.printf("%-16s %7s %10d %,20.2f %12s %,17.2f \n", chuXe, hangXe, dungTich, triGia,mauXe, TienThue());
    }




}
