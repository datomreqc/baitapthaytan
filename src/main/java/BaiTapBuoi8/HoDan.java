package BaiTapBuoi8;

import java.util.Scanner;

public class HoDan
{
    private int soTVien;
    private int soNha;
    private ThanhVien[] thanhVien;

    public HoDan()
    {
        this.thanhVien=new ThanhVien[10];
    }
    public HoDan(int soTVien, int soNha, ThanhVien[] thanhVien)
    {
        this.soTVien=soTVien;
        this.soNha=soNha;
        this.thanhVien=thanhVien;
    }

    public void nhapThongTin(Scanner sc)
    {
        System.out.print("Nhập số thành viên: ");
        this.soTVien=sc.nextInt();
        System.out.print("Nhập số nhà: ");
        this.soNha=sc.nextInt();sc.nextLine();
        for(int i=0;i<this.soTVien;i++)
        {
            System.out.println("Nhập thông tin thành viên thứ "+(i+1)+": ");
            thanhVien[i]=new ThanhVien();
            thanhVien[i].nhapThongTin(sc);
        }
    }
    public void hienThongTin()
    {
        System.out.println("Số nhà: "+this.soNha);
        System.out.println("Số thành viên: "+this.soTVien);
        for(int i=0;i<this.soTVien;i++)
        {
            System.out.println("Thành viên thứ "+(i+1)+": ");
            thanhVien[i].hienThongTin();
        }
    }
    public ThanhVien[] getThanhVien()
    {
        return this.thanhVien;
    }
}
