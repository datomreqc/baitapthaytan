package Buoi4_ControlFlows;

public class ControlFlows {
    //income = 120_000
    // HasHighIncome >100_000 thu nhập cao
    public static void main(String[] args){
        int income = 120_000;
        boolean HasHighIncome;
        if (income>100_000) {
            HasHighIncome =true;
        } else{
            HasHighIncome = false;
        }
        System.out.println(HasHighIncome);


        String className;
        if (income>100_000)
            className = "First";
        else
            className = "Second";
        // ternary operator
        String classname2 = income >100_000 ? "First" : "Second";
    }
}
