package Buoi4_ControlFlows;
import java.util.*;
public class Bai18 {

    // function to check if brackets are balanced
    static boolean Kiemtrakitu (String expr)
    {
        Deque<Character> stack = new ArrayDeque<Character>();

        for (int i = 0; i < expr.length(); i++)
        {
            char x = expr.charAt(i);

            if (x == '(' || x == '[' || x == '{')
            {
                stack.push(x);
                continue;
            }

            if (stack.isEmpty())
                return false;
            char check;
            switch (x) {
                case ')':
                    check = stack.pop();
                    if (check == '{' || check == '[')
                        return false;
                    break;

                case '}':
                    check = stack.pop();
                    if (check == '(' || check == '[')
                        return false;
                    break;

                case ']':
                    check = stack.pop();
                    if (check == '(' || check == '{')
                        return false;
                    break;
            }
        }

        return (stack.isEmpty());
    }

    public static void main(String[] args)
    {
        System.out.print("Moi ban nhap ki tu can kiem tra ");
        Scanner in = new Scanner(System.in);
        String expr= in.nextLine();

        if (Kiemtrakitu(expr))
            System.out.println("Balanced ");
        else
            System.out.println("Not Balanced ");
    }
}
