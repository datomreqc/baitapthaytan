package Buoi4_ControlFlows;
import java.util.Scanner;

public class Bai12 {
    // Nhập tháng và năm tính ra số ngày trong tháng năm đó
    private static Scanner sc;
    public static void main(String[] strings)
    {
        Scanner input = new Scanner(System.in);
        int month;
        sc = new Scanner(System.in);

        System.out.print("Nhập số tháng từ  1 tới 12  : ");
        month = sc.nextInt();
        System.out.print("Nhập số năm: ");
        int year = input.nextInt();

        if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12 )
        {
            System.out.println("Tháng " + month + " Của Năm " + year + " có 31 ngày" );
        }
        else if ( month == 4 || month == 6 || month == 9 || month == 11 )
        {
            System.out.println("Tháng " + month + " Của Năm " + year + " có 30 ngày");
        }
        else if ( month == 2 )

        {
            if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
                System.out.println("Tháng " + month + " Của Năm " + year + " có 29 ngày");
            } else {
                System.out.println("Tháng " + month + " Của Năm " + year + " có 28 ngày");
            }
        }
        else
            System.out.println("Vui lòng nhập đúng số tháng từ 1 tới 12");
    }
}

