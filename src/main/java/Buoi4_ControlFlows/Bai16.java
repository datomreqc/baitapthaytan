package Buoi4_ControlFlows;

import java.util.Scanner;

public class Bai16 {
    // Write a program in Java to reverse a number.
    // Function to reverse the number


    // Driver Function
    public static void main (String[] args) {
        int n, sodu, sodaonguoc = 0;
        System.out.print("Nhap vao mot so nguyen : n=");
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();

        while(n>0){

            sodu = n%10;
            sodaonguoc = (sodaonguoc*10) + sodu;
            n = n/10;
        }

        System.out.print("Số đảo ngược của n là: "+ sodaonguoc);
    }
}
