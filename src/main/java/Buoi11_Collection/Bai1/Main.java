package Buoi11_Collection.Bai1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    static ArrayList<SinhVien> danhSachSV = new ArrayList<>();
    static ArrayList<MonHoc> danhSachMH = new ArrayList<>();
    static ArrayList<Diem> danhSachDiem = new ArrayList<>();

    public boolean kiemTraTonTaiSV(SinhVien sv) {
        return this.danhSachSV.contains(sv);
    }

    public boolean kiemTraTonTaiMH(MonHoc mh) {
        return this.danhSachMH.contains(mh);
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int luaChon = 0;
        do {
            System.out.println("MENU ---------- ");
            System.out.println("Vui lòng chọn chức năng: ");
            System.out.println(
                    "1.	Danh sách sinh viên.\n"
                            + "2.	Nhập thông tin sinh viên mới.\n"
                            + "3.	Tìm kiếm sinh viên theo mã sinh viên. \n"
                            + "4.	Danh sách môn học.\n"
                            + "5.	Nhập thông tin môn học mới.\n"
                            + "6.	Nhập điểm môn học cho sinh viên.\n"
                            + "7.	Cập nhật điểm cho sinh viên.\n"
                            + "8.	Xem điểm sinh viên theo mã sinh viên.\n"
                            + "9.	In ra danh sách sinh viên theo các mã môn học.\n"
                            + "10.	In ra sinh viên có điểm trung bình tất cả các môn cao nhất.\n"
                            + "0.   Thoát khỏi chương trình");
            luaChon = sc.nextInt();
            sc.nextLine();

            if (luaChon == 1) {
                System.out.println("Danh Sách Sinh Viên");
                for (SinhVien sinhVien : danhSachSV) {
                    System.out.println(sinhVien);
                }
            } else if (luaChon == 2) {
//				1.	Thêm sinh viên vào danh sách.
                SinhVien sv = new SinhVien();
                System.out.println("Nhập mã sinh viên: ");
                sv.setMaSinhVien(sc.nextLine());
                System.out.println("Nhập tên sinh viên: ");
                sv.setTenSinhVien(sc.nextLine());
                System.out.println("Nhập ngày sinh của sinh viên: ");
                sv.setNgaySinh(sc.nextLine());
                System.out.println("Nhập quê quán của sinh viên: ");
                sv.setQueQuan(sc.nextLine());
                danhSachSV.add(sv);

            } else if (luaChon == 3) {
//				3.	Tìm kiếm sinh viên theo mã sinh viên.
                System.out.println("Nhập mã sinh viên cần tìm: ");
                String MSV = sc.nextLine();
                for (SinhVien sinhVien : danhSachSV) {
                    if (sinhVien.getMaSinhVien().indexOf(MSV) >= 0) {
                        System.out.println("Thông tin sinh viên cần tìm là: ");
                        System.out.println(sinhVien);
                    } else {
                        System.out.println("Không tồn tại sinh viên này trong danh sách SV");
                    }
                }
            } else if (luaChon == 4) {
//				4.	In ra Danh sách môn học.
                System.out.println("Danh Sách Môn Học");
                for (MonHoc monHoc : danhSachMH) {
                    System.out.println(monHoc);
                }
            } else if (luaChon == 5) {
//				5.	Nhập thông tin môn học mới.
                MonHoc mh = new MonHoc();
                System.out.println("Nhập mã môn học: ");
                mh.setMaMonHoc(sc.nextLine());
                System.out.println("Nhập tên môn học: ");
                mh.setTenMonHoc(sc.nextLine());
                danhSachMH.add(mh);
            } else if (luaChon == 6) {
//				6.	Nhập điểm môn học cho sinh viên.
                Diem diem = new Diem();
                System.out.println("Nhập mã sinh viên: ");
                String msv = sc.nextLine();
                diem.setmSV(new SinhVien(msv));
                for (SinhVien sinhVien : danhSachSV) {
                    if (sinhVien.getMaSinhVien().equals(msv)) {
                        System.out.println("Nhập mã môn học: ");
                        String mMH = sc.nextLine();
                        diem.setmMH(new MonHoc(mMH));
                        for (MonHoc monHoc : danhSachMH) {
                            if (monHoc.getMaMonHoc().equals(mMH)) {
                                System.out.println("Nhập điểm môn học: ");
                                diem.setDiemSo(Integer.parseInt(sc.nextLine()));
                            }

                        }
                    } else {
                        System.out.println("Không tìm thấy sinh viên");
                    }
                }
                danhSachDiem.add(diem);
                System.out.println(danhSachDiem);


            } else if (luaChon == 7) {
//				7.	Cập nhật điểm cho sinh viên.
                System.out.println("Nhập mã sinh viên");
                String msv = sc.nextLine();
                for (Diem diem : danhSachDiem) {
                    if (!msv.equals(diem.getmSV().getMaSinhVien())) {
                        System.out.println("Không tồn tại sinh viên này trong danh sách SV");
                    } else {
                        System.out.println("Vui lòng cập nhật điểm bạn mong muốn: ");
                        int diemMoi = sc.nextInt();
                        diem.setDiemSo(diemMoi);
                    }
                }
                System.out.println(danhSachDiem);


            } else if (luaChon == 8) {
//				8.	Xem điểm sinh viên theo mã sinh viên.
                System.out.println("Nhập mã sinh viên");
                String msv = sc.nextLine();
                for (Diem diem : danhSachDiem) {
                    if (!msv.equals(diem.getmSV().getMaSinhVien())) {
                        System.out.println("Không tồn tại sinh viên này trong danh sách SV");
                    } else {
                        System.out.println("Thông tin sinh viên cần tìm là: ");
                        System.out.println(diem.toString());
                    }
                }


            } else if (luaChon == 9) {
//				9.	In ra danh sách sinh viên theo các mã môn học..
                System.out.println("Nhập mã môn học");
                String mmh = sc.nextLine();
                for (Diem diem : danhSachDiem) {
                    if (!mmh.equals(diem.getmMH().getMaMonHoc())) {
                        System.out.println("Không tồn tại sinh viên này trong danh sách SV");
                    } else {
                        for (SinhVien sv : danhSachSV) {
                            if (diem.getmSV().equals(sv.getMaSinhVien())) {
                                System.out.println(sv.toString());
                            }
                        }
                    }
                }

            } else if (luaChon == 10) {
//				10.	In ra sinh viên có điểm trung bình tất cả các môn cao nhất.
                int tongMonHoc = danhSachMH.size();

                SinhVien svDiemTBMax = null;
                float diemSoSanh = 0;
                float diemTB = 0;
                for (SinhVien sv : danhSachSV) {
                    int count = 0;
                    int tongDiem = 0;
                    for (Diem diem : danhSachDiem) {
                        if (sv.getMaSinhVien().equals(diem.getmSV().getMaSinhVien())) {
                            tongDiem += diem.getDiemSo();
                            count++;
                        }
                    }
                    diemTB = tongDiem / count;
                    if (diemTB > diemSoSanh) {
                        diemSoSanh = diemTB;
                        svDiemTBMax = sv;
                    }
                }
                System.out.println("Sinh viên có điểm TB lớn nhất với điểm TB = " + diemTB);
                System.out.println(svDiemTBMax.toString());


            }
        } while (luaChon != 0);
    }


}
