package Buoi11_Collection.Bai1;

public class Diem {
    private SinhVien mSV;
    private MonHoc mMH;
    private  int DiemSo;

    public Diem() {
    }

    public Diem(String maSinhVien, String maMonHoc, int diemSo) {
        this.mSV = new SinhVien(maSinhVien);
        this.mMH= new MonHoc(maMonHoc);
        this.DiemSo= diemSo;
    }

    public SinhVien getmSV() {
        return mSV;
    }

    public void setmSV(SinhVien mSV) {
        this.mSV = mSV;
    }

    public MonHoc getmMH() {
        return mMH;
    }

    public void setmMH(MonHoc mMH) {
        this.mMH = mMH;
    }

    public int getDiemSo() {
        return DiemSo;
    }

    public void setDiemSo(int diemSo) {
        DiemSo = diemSo;
    }

    @Override
    public String toString() {
        return "Diem{" +
                "mSV=" + mSV.getMaSinhVien() +
                "mMH=" + mMH.getMaMonHoc() +
                ", tMH=" + mMH.getTenMonHoc() +
                ", DiemSo=" + DiemSo +
                '}';
    }
}
