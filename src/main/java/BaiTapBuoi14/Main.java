package BaiTapBuoi14;

import com.github.javafaker.Faker;

import java.util.*;
import java.util.Comparator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static BaiTapBuoi14.Main.Gender.getRandomGender;

public class Main {
//    a. Có bao nhiêu nam trên 18 tuổi.
//    b. Có bao nhiêu nữ có tên đệm “thị”.
//    c. Lọc ra những người họ “Nguyễn” và sắp xếp theo thứ tự chữ cái.
//    d. In ra danh sách những người độ tuổi từ 20-30
//    e. Đếm số lượng thành phố có trong danh sách trên. (gợi ý: distinct)
//    f. Lấy ra 10 người đầu tiên trong danh sách (gợi ý: limit)
//    g. Tính độ tuổi trung bình.
//    h. Tính mức lương trung bình của nam ở độ tuổi từ 20-40
//    i. Sắp xếp danh sách theo độ tuổi.
//    j. In ra danh sách theo City – List<Human>

    public static void main(String[] args) {

        // Faker tạo ra 100 sinh viên
        Faker faker = new Faker(new Locale("vi", "VN"));
        List<Human> humanArrayList = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            Human hm = new Human();
            hm.setId(i);
            hm.setFirstname(faker.name().firstName());
            hm.setLastname(faker.name().lastName());
            hm.setCity(faker.address().city());
//            hm.setGender(faker.number().numberBetween(0,2));
            hm.setGender(getRandomGender().name());
            hm.setAge(faker.number().numberBetween(10, 51));
            hm.setSalary(faker.number().numberBetween(10, 3001));
            humanArrayList.add(hm);
        }
        for (Human hm : humanArrayList) {
            System.out.println(hm);
        }

//            a. Có bao nhiêu nam trên 18 tuổi.
        long namTren18Tuoi = humanArrayList.stream()
                .filter(hm -> hm.getGender().contains("Nam"))
                .filter(hm -> hm.getAge() > 18)
                .count();
        System.out.println("Số lượng sinh viên nam trên 18 tuổi là: " + namTren18Tuoi);


//        b. Có bao nhiêu nữ có tên đệm “thị”.
        long nuTemDemLaThi = humanArrayList.stream()
                .filter(hm -> hm.getGender().contains("Nữ"))
                .filter(hm -> hm.getLastname().contains("Thị"))
                .count();
        System.out.println("Số lượng sinh viên nữ có tên đệm là Thị là : " + nuTemDemLaThi);


//        c. Lọc ra những người họ “Nguyễn” và sắp xếp theo thứ tự chữ cái.
        List<Human> hoNguyen = humanArrayList.stream()
                .filter(hm -> hm.getLastname().equalsIgnoreCase("Nguyễn"))
                .sorted()
                .collect(Collectors.toList());
//        Collections.sort(hoNguyen);
        System.out.println("Những người họ nguyễn và sắp xếp theo chũ cái là: \n" + hoNguyen);


//         d. In ra danh sách những người độ tuổi từ 20-30
        List<Human> tuoi20Up30= humanArrayList.stream()
                .filter(hm -> hm.getAge() >= 20)
                .filter(hm -> hm.getAge() <= 30)
                .collect(Collectors.toList());
        System.out.println("danh sách những người độ tuổi từ 20-30 là: \n " + tuoi20Up30);


//        e. Đếm số lượng thành phố có trong danh sách trên. (gợi ý: distinct)
        // lấy ra danh sách thành phố -> đếm
        List<String> DSThanhPho = humanArrayList.stream()
                .map(hm -> hm.getCity())
                .distinct()
                .collect(Collectors.toList());
        System.out.println("Tổng Thành Phố là: " + DSThanhPho.size());

        long listThanhPho = humanArrayList.stream()
                .filter(distinctByKey(p -> p.getCity()))
                .count();
      System.out.println("Số lượng thành phố có trong danh sách\n"+ listThanhPho);


//        f. Lấy ra 10 người đầu tiên trong danh sách (gợi ý: limit)
        List<Human> limit = humanArrayList.stream()
                .limit(10)
                .collect(Collectors.toList());
        System.out.println("10 người đầu tiên trong dánh sách là : \n " + limit);

//        g. Tính độ tuổi trung bình.
        double tuoiTrungBinh = humanArrayList.stream()
                .collect(Collectors.averagingDouble(hm -> hm.getAge()));
        System.out.println("Tuổi Trung Bình là: " + tuoiTrungBinh);
        double tuoiTB = calHuman((a) -> {
            double t = 0;
            for (int i = 0; i < a.size(); i++) {
                t += a.get(i).getAge();
            }
            return t / a.size();
        }, humanArrayList);
        System.out.println("Điểm trung bình của các sinh viên là: " + tuoiTB);


//        h. Tính mức lương trung bình của nam ở độ tuổi từ 20-40
        List<Human> namtuoi20uo40= humanArrayList.stream()
                .filter(hm -> hm.getGender().contains("Nam"))
                .filter(hm -> hm.getAge() > 20)
                .filter(hm -> hm.getAge() < 40)
                .collect(Collectors.toList());
        double luongTB = calHuman((a) -> {
            double t = 0;
            for (int i = 0; i < a.size(); i++) {
                t += a.get(i).getSalary();
            }
            return t / a.size();
        }, humanArrayList);
        Double luongTrungBinh= humanArrayList.stream()
                .filter(hm -> hm.getGender().contains("Nam"))
                .filter(hm -> hm.getAge() > 20)
                .filter(hm -> hm.getAge() < 40)
                .collect(Collectors.averagingDouble(hm -> hm.getSalary()));
        System.out.println("Lương trung bình là: "+luongTrungBinh);
        System.out.println("Lương trung bình của nam ở độ tuổi từ 20-40 là: " + luongTB);

//        j. Sắp xếp danh sách theo độ tuổi.
        List<Human> listSortAge = sortHuman(Comparator.comparingInt(Human::getAge), humanArrayList);
        System.out.println("Danh sách theo độ tuổi là: " +listSortAge);

        //In ra danh sách theo City – List<Human>
        // in danh sách theo city - list (GROUP BY)
        // 1 thành phố -> N người map (KEY, Value) -> MAP (City, list<Human>)
        Map<String, List<Human>> map = humanArrayList.stream()
                .collect(Collectors.groupingBy(hm -> hm.getCity()));
        System.out.println("Danh sách Human theo thành phố là: ");

        for (Map.Entry<String, List<Human>> entry : map.entrySet()) {
            System.out.println("Country " + entry.getKey());
            entry.getValue().stream().forEach(e-> System.out.println(e));
//            System.out.println(entry.getValue());
        }

//        humanArrayList.stream()
//                .sorted(o1, o2)->{
//            if (o1.get)
//        }

    }

    public enum Gender {
        Nam,
        Nữ;

        public static Gender getRandomGender() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }
    }
    public static double calHuman(Function<List<Human>, Double> fn, List<Human> listHuman) {
        return fn.apply(listHuman);
    }
    public static List<Human> sortHuman(Comparator comparator, List<Human> humanList) {
        return (List<Human>) humanList.stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }
    public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }




}
