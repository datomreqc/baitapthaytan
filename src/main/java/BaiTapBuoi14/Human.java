package BaiTapBuoi14;

import java.text.Collator;;
import java.util.Locale;

public class Human implements Comparable<Human> {
    private int id;
    private String firstname;
    private String lastname;
    private String city;
    private String gender;
    private int age;
    private int salary;

    public Human() {
    }

    public Human(int id, String firstname, String lastname, String country, String gender, int age, int salary) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.city = country;
        this.gender = gender;
        this.age = age;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Human{" +
                "id=" + id +
                ", fulltname='" + lastname + " "+ firstname + '\'' +
//                ", lastname='" + lastname + '\'' +
                ", city='" + city + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", salary=" + salary +
                '}';
    }

    @Override
    //So sánh đối tượng hiện tại và tương lai
    public int compareTo(Human human) {
//        String thisFullname = lastname + " " + firstname;
//        String oFullname = human.lastname + " " + human.firstname;
        Collator vnCollator = Collator.getInstance(new Locale("vi"));
        vnCollator.setStrength(Collator.PRIMARY);
        String fullNameSort = human.getFirstname().concat(" ").concat(human.getLastname());
        String fullNameRoot = this.firstname.concat(" ").concat(this.lastname);
        return vnCollator.compare(fullNameRoot, fullNameSort);

    }

}

