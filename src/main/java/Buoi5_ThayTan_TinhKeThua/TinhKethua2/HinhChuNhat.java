package Buoi5_ThayTan_TinhKeThua.TinhKethua2;

public class HinhChuNhat {
    private double chieuDai;
    private double chieuRong;
    public HinhChuNhat(double chieuDai, double chieuRong) {
        this.chieuDai = chieuDai;
        this.chieuRong = chieuRong;
    }
    public double getChieuDai() {
        return chieuDai;
    }
    public double getChieuRong() {
        return chieuRong;
    }
    public double getChuVi(){
        return (chieuDai + chieuRong) * 2;
    }
    public double getDienTich(){
        return chieuDai * chieuRong;
    }
    @Override
    public String toString() {
        return getClass().getSimpleName()+"[" +
                "chieuDai=" + chieuDai +
                ", chieuRong=" + chieuRong +
                ", chuVi="+getChuVi()+
                ", dienTich="+ getDienTich()+
                ']';
    }
}
