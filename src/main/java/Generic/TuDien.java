package Generic;

import javax.xml.crypto.dsig.keyinfo.KeyValue;

public class TuDien {
    public static void main(String[] args) {
        KeyValuePair<String, Integer> entry = new KeyValuePair<String, Integer>("Hello Xin Chào", 10);
        String name = entry.getKey();
        Integer id = entry.getValue();
        System.out.println("Name = " + name + ", Id = " + id); //
        KeyValuePair<String, String> entry1 = new KeyValuePair<String, String>("PC1", "192.016.1.1");
        String PC = entry1.getKey();
        String IP = entry1.getValue();
        System.out.println("name PC = " + PC + ", địa chỉ IP = " + IP);
        KeyValuePair<String, String>[] tuDien = new  KeyValuePair[100];
        tuDien[0] = new KeyValuePair<>("Xin chao", "Tôi là Đạt");
        tuDien[1] = new KeyValuePair<>("Bye", "về nghỉ lễ thôi anh em");
        for (KeyValuePair item: tuDien){
            if (item==null)
                break;
            System.out.println(item.toString());

        }
    }
}


