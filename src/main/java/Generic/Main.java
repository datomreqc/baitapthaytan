package Generic;

public class Main {
    public static void main(String[] args) {
        // Box lưu 1 số
        Box<Double> box1 = new Box<>();
        box1.setT(1.5);

        Box<String> box2 = new Box<>();
        box2.setT("Đat");
        System.out.println(box1);
        System.out.println(box2);

        // Box 3 có thể chưa được đối tượng hoaQua
        Box<HoaQua> box3 = new Box<>(new HoaQua(10, "chuoi","den"));
        System.out.println(box3.toString());
        // Box4 chứa được box 3.
        Box<Box<HoaQua>> box4= new Box<>();
        box4.setT(box3);
        System.out.println(box4.getT().getT().mauSac);
        System.out.println(box4.getT().getT().getClass().getSimpleName());
//        Box q = box4.getT().getT().getClass().getClass();

    }
}
