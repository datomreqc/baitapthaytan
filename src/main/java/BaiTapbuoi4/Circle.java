package BaiTapbuoi4;
import java.lang.Math;


public class Circle {

    private double radius;

    public Circle() {  // 1st (default)
        radius = 1.0;
    }

    public Circle(double r) {  // 2nd constructor
        radius = r;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return radius*radius*Math.PI;
    }
    public double chuVi() {
        return 2*radius*Math.PI;
    }

    public String toString(){//ghi de phuong thuc toString()
        return "Bán kính hình tròn là:" + radius+ " " + "Diện tích hình tròn là: " + getArea()+ " "+"Chu vi hình tròn là: "+ " "+chuVi();
    }

}
