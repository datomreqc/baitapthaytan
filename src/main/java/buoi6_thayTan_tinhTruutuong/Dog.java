package buoi6_thayTan_tinhTruutuong;

public class Dog extends Animal{
    public Dog(String name, String color, String size) {
        super(name, color, size);
    }

    @Override
    public void makeSound() {
        System.out.println("Gâu Gâu");
    }

    @Override
    public void move() {
        System.out.println("Di chuyển bằng 4 chân");

    }
}
