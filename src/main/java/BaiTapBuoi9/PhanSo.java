package BaiTapBuoi9;
import java.util.Scanner;
public class PhanSo {
    private int tuSo;
    private int mauSo;

    public PhanSo() {
        tuSo=1;
        mauSo=1;

    }

    public PhanSo(int tuSo, int mauSo) {
        this.tuSo = tuSo;
        this.mauSo = mauSo;
    }

    public int getTuSo() {
        return tuSo;
    }

    public int getMauSo() {
        return mauSo;
    }

    public void setTuSo(int tuSo) {
        this.tuSo = tuSo;
    }

    public void setMauSo(int mauSo) {
        this.mauSo = mauSo;
    }

    public int timUSCLN(int a, int b) {
        while (a != b) {
            if (a > b) {
                a -= b;
            } else {
                b -= a;
            }
        }
        return a;
    }

    public void toiGianPhanSo() {
        int i = timUSCLN(this.getTuSo(), this.getMauSo());
        this.setTuSo(this.getTuSo() / i);
        this.setMauSo(this.getMauSo() / i);
    }

    public boolean kiemTraPhanSo(int tuso, int mauSo)
    {
        if(mauSo == 0) {
            System.out.println("Mau so phai khac 0");
            return  false;
        }
        try {

            Integer.parseInt(String.valueOf(tuso));
            Integer.parseInt(String.valueOf(tuso));
            return true;
        }
        catch(NumberFormatException e) {
            System.out.println("Ban khong duoc nhap chuoi");
        }
        return  true;
    }
    static int nhapgt(){
        Scanner in = new Scanner(System.in);
        String str;
        boolean isInteger = false;
        int so  = 0;
        while (!isInteger) {
            try {
                str = in.nextLine();
                so = Integer.valueOf(str).intValue();
                isInteger = true;
            } catch (NumberFormatException ex) {
                System.out.println("Ban phai nhap so nguyen. Moi ban nhap lai");
            }
        }
        return so;
    }
    public void nhap() {
        Scanner sc = new Scanner(System.in);
        int ts, ms;
        do {
            System.out.print("\tNhap vao tu so: ");
            ts = nhapgt();
            System.out.print("\tNhap vao mau so: ");
            ms = nhapgt();

            if (ms == 0) {
                System.out.println("Nhap lai thong tin cho phan so!");
            }
        } while (ms == 0);
        tuSo = ts;
        mauSo = ms;
    }

    @Override
    public String toString() {
        return "PhanSo{" +
                "tuSo=" + tuSo + "/"+
                ", mauSo=" + mauSo +
                '}';
    }
}