package BaiTapBuoi9;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main1 {
    public static PhanSo[] nhapPhanSo(int soLuongPhanSo) {
        Scanner scanner = new Scanner(System.in);
        PhanSo lstPS[] = new PhanSo[soLuongPhanSo];
        for (int i = 0; i < soLuongPhanSo; i++) {
            System.out.println("Mời bạn nhập phân số thứ: " + (i + 1));
            PhanSo ps = new PhanSo();
            ps.nhap();
            lstPS[i] = ps;
        }
        return lstPS;
    }

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        TinhToan tt = new TinhToan();
        List<PhanSo> lstKQ = new ArrayList<>();
        int choiceNumber;
        FileOutputStream outputStream  = new FileOutputStream("C:\\Users\\dat.nguyen3_onemount\\IdeaProjects\\untitled\\java\\main\\java\\BaiTapBuoi9\\result.txt");
        DataOutputStream outputStreamWriter  = new DataOutputStream(outputStream );
        do {
            System.out.println("---------------MENU-------------");
            System.out.println("1. Tính Tổng 3 phân số.");
            System.out.println("2. Tính Hiệu 3 phân số.");
            System.out.println("3. Tính Tích 2 phân số");
            System.out.println("4. Tính Thương 2 phân số");
            System.out.println("5. Lịch sử 5 phép tính gần nhất");

            choiceNumber = Integer.parseInt(scanner.nextLine());
            try {

                switch (choiceNumber) {
                    case 1:
                        PhanSo congPS = tt.cong(nhapPhanSo(3));
                        lstKQ.add(congPS);
                        System.out.println(congPS);
                        System.out.println("Tổng 3 phân số là: " + congPS.getTuSo() + "/" + congPS.getMauSo());
                        outputStreamWriter .writeBytes(congPS.getTuSo()+"/"+congPS.getMauSo());
                        outputStreamWriter.flush();
                        outputStreamWriter.writeBytes("\n");
                        break;
                    case 2:
                        PhanSo truPS = tt.tru(nhapPhanSo(3));
                        lstKQ.add(truPS);
                        System.out.println("Hiệu 3 phân số là: " + truPS.getTuSo() + "/" + truPS.getMauSo());
                        outputStreamWriter .writeBytes(truPS.getTuSo()+ "/"+truPS.getMauSo());
                        outputStreamWriter.flush();
                        outputStreamWriter.writeBytes("\n");
                        break;
                    case 3:
                        PhanSo nhanPS = tt.nhan(nhapPhanSo(2));
                        lstKQ.add(nhanPS);
                        System.out.println("Tích 2 phân số là: " + nhanPS.getTuSo() + "/" + nhanPS.getMauSo());
                        outputStreamWriter .writeBytes(nhanPS.getTuSo()+ "/"+nhanPS.getMauSo());
                        outputStreamWriter.flush();
                        break;
                    case 4:
                        PhanSo chiaPS = tt.chia(nhapPhanSo(2));
                        lstKQ.add(chiaPS);
                        System.out.println("Thuong 2 phan so la: " + chiaPS.getTuSo() + "/" + chiaPS.getMauSo());
                        outputStreamWriter .writeBytes(chiaPS.getTuSo()+ "/"+chiaPS.getMauSo());
                        outputStreamWriter.flush();

                        break;
                    case 5:
                        System.out.println("Ket qua cac phep tinh la: ");
                        for (int i = 0; i <lstKQ.size(); i++) {
                            System.out.println(lstKQ.get(i).getTuSo() + "/" + lstKQ.get(i).getMauSo());
                        }
                        System.out.println("Lich su 5 phep tinh gan nhat");
                        try {
                            FileInputStream fis = new FileInputStream("C:\\Users\\dat.nguyen3_onemount\\IdeaProjects\\untitled\\java\\main\\java\\BaiTapBuoi9\\result.txt");
                            StringBuilder resultStringBuilder = new StringBuilder();
                            try (BufferedReader br
                                         = new BufferedReader(new InputStreamReader(fis))) {
                                String line;
                                var count = 0;
                                while ((line = br.readLine()) != null && count < 5) {
                                    // resultStringBuilder.append(line).append("\n");
                                    System.out.println("Lịch sử các phép tính: " + line);
                                    count++;
                                }
                            }

                            fis.close();

                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        break;
                    default:
                        System.out.println("Khong hop le");
                        break;
                }
            } catch (NumberFormatException e) {
                System.out.println("Lua chon menu khong dung");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (choiceNumber != 5);
        scanner.close();

    }
}

