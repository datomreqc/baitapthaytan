package Buoi3Array;

import java.lang.reflect.Array;
import java.util.Arrays;

public class MyArray {

    public static void main(String[] args) {

        int[] numbers = new int[5]; // array có 5 phần tử

        numbers[0] = 1; // số đầu tiên bắt đầu từ index = 0
        numbers[1] = 2;
        numbers[2] = 3;


        System.out.println(Arrays.toString(numbers)); // khi khởi tạo mà chưa gán giá trị thì các phần tử chưa được gán sẽ mặc định đưa về 0
        System.out.println(numbers.length);
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));
        System.out.println(Arrays.toString(numbers));
    }
}
