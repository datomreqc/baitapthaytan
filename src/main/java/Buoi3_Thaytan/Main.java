package Buoi3_Thaytan;

public class Main {
    public static void main(String[] args){
        // tạo 1 đối tượng dog
        Dog dog1 = new Dog();
        dog1.breed="Pitbull";
        dog1.age =1;

        dog1.size="normal";
        dog1.color="black-while";

        // thực hiện 1 hành động
        dog1.eat();
        dog1.run();
        dog1.sleep();

        // Tạo đối tượng dog2
        Dog dog2= new Dog();
        dog2.breed="Chow chow";
        dog2.age =3;

        dog2.size="Midium";
        dog2.color="Brown";

        dog2.eat();
        dog2.run();
        dog2.sleep();


    }
}
