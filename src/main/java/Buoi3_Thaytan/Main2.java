package Buoi3_Thaytan;
import java.util.ArrayList;
import java.util.ArrayList;
public class Main2 {
    public static void main(String[] args) {
        // ArrayList
        ArrayList<HinhChuNhat> danhSach = new ArrayList<>();
        danhSach.add(new HinhChuNhat(3, 4));
        danhSach.add(new HinhChuNhat(7, 8));
        danhSach.add(new HinhChuNhat(8, 6));
        HinhChuNhat hcn = new HinhChuNhat();
        hcn.chieuDai = 6;
        hcn.chieuRong = 7;
        danhSach.add(hcn);
        // In ra chu vi và diện tích từng hình
        for (HinhChuNhat item: danhSach) {
            System.out.printf("Chu vi: %.2f cm, diện tích %.2f cm2 \n", item.chuVi(), item.dienTich());
        }
    }
}