package Buoi6;
import java.util.Scanner;
public class HCN1 {
    public static void main(String[] args) {
        double a = 0, b = 0;
        Scanner in = new Scanner(System.in);
        while (true) {
            System.out.print("Nhập chiều dài: ");
            a = in.nextDouble();
            System.out.print("Nhập chiều rộng: ");
            b = in.nextDouble();
            if( a > 0 && b > 0) {
                break;
            }
            else {
                System.out.println("Các cạnh phải là số dương");
            }
        }
        in.close();
        double cv = (a+b)*2;
        System.out.printf("Chu vi là %.2f \n", cv);
        System.out.println("Kết thúc chương trình");
    }
}