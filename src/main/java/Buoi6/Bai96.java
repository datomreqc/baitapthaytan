package Buoi6;

import java.util.Scanner;

public class Bai96 {
    //Viết chương trình nhập giá trị x sau khi tính giá trị của hàm số:
    //𝑓(𝑥) = {2𝑥*x + 5𝑥 + 9 𝑘ℎ𝑖 𝑥 ≥ 5
    //𝑓(𝑥) =-2x*x + 4𝑥 - 9 𝑘ℎ𝑖 𝑥 <5
    public static void main(String[] args) {
        float ketqua ;
        float x;
        Scanner sc;

        System.out.println("Nhập giá trị x = ");
        sc = new Scanner(System.in);
        x = sc.nextInt();
        if (x>=5) {
            ketqua = 2 * x * x + 5*x + 9;
            System.out.println("Kết quả của hàm số với x = "+ x +" là " + ketqua);
        }else {
            ketqua = -2 * x * x + 4*x - 9;
            System.out.println("Kết quả của hàm số với x = "+ x +" là " + ketqua);
        }
        sc.close();
    }
}
