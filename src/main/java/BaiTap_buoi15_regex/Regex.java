package BaiTap_buoi15_regex;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Regex {
    public static void main(String[] args) {
        final String regex = "^(03|08|09)?[2-9]";
        final String string = "086\n"
                + "096\n"
                + "097\n"
                + "098\n"
                + "032\n"
                + "033\n"
                + "034\n"
                + "035\n"
                + "036\n"
                + "037\n"
                + "038\n"
                + "039\n";

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(string);

        while (matcher.find()) {
            System.out.println("Full match: " + matcher.group(0));

            for (int i = 1; i <= matcher.groupCount(); i++) {
                System.out.println("Group " + i + ": " + matcher.group(i));
            }
        }
        final String regex1 = "^(0|\\+84|\\\\(\\+84\\\\))(\\s|\\.|\\-)?((3[2-9])|(8[6])|(9[67]))(\\d)?(\\s|\\.|\\-)?(\\d{3,4})(\\s|\\.|\\-)?(\\d{3})$";
        final String string1 = "0861238888\n"
                + "0964444555\n"
                + "0321123456\n"
                + "0398272388\n"
                + "(+84)973456766\n"
                + "+84-973456767\n"
                + "038.1234.999\n"
                + "+84-361-223-454\n"
                + "+84 971 234 888\n"
                + "861238888\n"
                + "840321123456\n"
                + "(+84)0321123456\n"
                + "0839738123";

        final Pattern pattern1 = Pattern.compile(regex1, Pattern.MULTILINE);
        final Matcher matcher1 = pattern1.matcher(string1);

        while (matcher1.find()) {
            System.out.println("Full match: " + matcher1.group(0));

            for (int i = 1; i <= matcher1.groupCount(); i++) {
                System.out.println("Group " + i + ": " + matcher1.group(i));
            }
        }
    }
}


