package BaiTap_buoi15_regex;

import java.util.Scanner;
import java.util.regex.Pattern;

public class CheckPhoneVietTel {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true){
            System.out.println("Mời bạn nhập số điện thoại: ");
            String phone = sc.nextLine();
            final String regex = "^(0|\\+84|\\(\\+84\\))(\\s|\\.|\\-)?((3[2-9])|(8[6])|(9[67]))(\\d)?(\\s|\\.|\\-)?(\\d{3,4})(\\s|\\.|\\-)?(\\d{3})$";
            Pattern p = Pattern.compile(regex, Pattern.MULTILINE);
            if (p.matcher(phone).find()){
                System.out.println("Đây là số điện thoại nhà mạng VIETTEL");
            }else {
                System.err.println("Đây không phải là số điện thoại nhà mạng VIETTEL");
            }
        }
    }
}
