package baiTapInterface.Bai1;

public class ResizableCircle extends Circle implements Resizable {

    public ResizableCircle(double radius) {
        super(radius);
    }

    @Override
    public String toString() {
        return "ResizableCircle{" +
                "radius=" + radius +
                '}';
    }


    @Override
    public void resize(int present) {
        radius *= present / 100.0;
        System.out.println(radius);
    }
}
