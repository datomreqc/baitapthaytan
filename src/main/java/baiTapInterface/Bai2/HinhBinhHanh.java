package baiTapInterface.Bai2;

public class HinhBinhHanh implements HinhHoc2D{
    private double canhDay;
    private double canhBen;
    private double chieuCao;

    public HinhBinhHanh(double canhDay, double canhBen, double chieuCao) {
        this.canhDay = canhDay;
        this.canhBen = canhBen;
        this.chieuCao = chieuCao;
    }

    public HinhBinhHanh(double canhDay, double chieuCao) {
        this.canhDay = canhDay;
        this.chieuCao = chieuCao;
    }

    public double getChieuCao() {
        return chieuCao;
    }

    public void setChieuCao(double chieuCao) {
        this.chieuCao = chieuCao;
    }

    public double getCanhDay() {
        return canhDay;
    }

    public void setCanhDay(double canhDay) {
        this.canhDay = canhDay;
    }

    public double getCanhBen() {
        return canhBen;
    }

    public void setCanhBen(double canhBen) {
        this.canhBen = canhBen;
    }

    @Override
    public double tinhChuVi() {
        double cv=(canhBen+canhDay)*2;
        return cv;
    }

    @Override
    public double tinhDienTich() {
        double dt= canhDay*chieuCao;
        return dt;
    }

    @Override
    public String toString() {
        if (canhBen!=0)
            return "HinhBinhHanh{" +
                    "canhDay=" + canhDay +
                    ", canhBen=" + canhBen +
                    ", chieuCao=" + chieuCao +
                    '}';
        else return "HinhBinhHanh{" +
                "canhDay=" + canhDay +
                ", chieuCao=" + chieuCao +
                '}';


    }
}
