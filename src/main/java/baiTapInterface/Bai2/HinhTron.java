package baiTapInterface.Bai2;

public class HinhTron implements HinhHoc2D{
    private double banKinh;

    public HinhTron(double banKinh) {
        this.banKinh = banKinh;
    }

    public double getBanKinh() {
        return banKinh;
    }

    public void setBanKinh(double banKinh) {
        this.banKinh = banKinh;
    }

    @Override
    public double tinhChuVi() {
        double cv = 2*banKinh*Math.PI;
        return cv;
    }

    @Override
    public double tinhDienTich() {
        return banKinh*banKinh*Math.PI;
    }

    @Override
    public String toString() {
        return "HinhTron{" +
                "banKinh=" + banKinh +
                '}';
    }
}
