package org.example;

import java.util.Date;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        byte age = 31;
        short temp = 21;
        age = 35;
        byte datAge = age;
        System.out.println(datAge);

        int count = 123_456_789; //123456789 có thể dùng _ để cách các con số cho code nó sáng hơn
        long c = 3_123_456_789L; // thêm chữ L or l báo cho java biết đây là số long ( nếu k thêm L java hiểu là int)
        double price = 9.99;
        float price2 = 9.99F; // java tự gán 9.99 sang số thập phân cần khai thêm chữ F or f

            //  char kiểu chữ cái
        char letter = 'A';  // cếu chỉ có 1 chữ cái dùng 1 ngoạc ''
            // boolean
        boolean isDone = true; // có thể là false or true

            // referance types

        Date  now = new Date(); // now thực tế là 1 oject của date
        System.out.println(now);

        System.out.println(now.getDate());
        System.out.println(now.getTime());  //time quy ra ms (m giây) tính từ năm 1970


        //

    }
}
