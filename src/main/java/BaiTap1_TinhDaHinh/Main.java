package BaiTap1_TinhDaHinh;

public class Main {
    public static void main(String[] args) {

        Circle c1 = new Circle(3, "Green",false);
        System.out.println(c1);
        System.out.println("Diện tích hình tròn là: ");
        System.out.println(c1.getArea());
        System.out.println("Chu vi hình tròn là: ");
        System.out.println(c1.getPerimeter());
        System.out.println("Màu: ");
        System.out.println(c1.getColor());
        System.out.println("Filled là: ");
        System.out.println(c1.isFilled());
        System.out.println("Bán Kính hình tròn là: ");
        System.out.println(c1.getRadius());
        //
        Rectangle r1 = new Rectangle(3,4,"Red",false) ;
        System.out.println(r1);
        System.out.println("Diện tích hình chữ nhật là: ");
        System.out.println(r1.getArea());
        System.out.println("Chu vi hình chữ nhật là: ");
        System.out.println(r1.getPerimeter());
        System.out.println("Màu: ");
        System.out.println(r1.getColor());
        System.out.println("Filled là: ");
        System.out.println(r1.isFilled());
        System.out.println("Chiều rộng HCN là: ");
        System.out.println(r1.getWidth());
        System.out.println("Chiều rộng HCN là: ");
        System.out.println(r1.getLength());
        //
        Shape s1 = new Circle(2, "Black", false);  // Upcast Circle to Shape
        System.out.println(s1);                   // which version?
        System.out.println("Diện tích hình tròn là: ");
        System.out.println(((Circle) s1).getArea());    // which version?
        System.out.println("Chu vi hình tròn là: ");
        System.out.println(((Circle) s1).getPerimeter());  // which version?
        System.out.println("Màu: ");
        System.out.println(s1.getColor());
        System.out.println("Filled là: ");
        System.out.println(s1.isFilled());
        System.out.println("Bán Kính hình tròn là: ");
        System.out.println(((Circle) s1).getRadius());



    }
}
